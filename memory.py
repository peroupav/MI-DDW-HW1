from urllib.parse import urlparse, urldefrag
from collections import deque


class Memory:
    def __init__(self, first_page, domain=None):
        self.domain = domain

        self.visited = []
        self.stacked = deque()
        self.ignored_extensions = ['pdf', 'jpg', 'png']

        self.stacked.append(first_page)

    def add_link(self, link):
        # normalize
        link = urldefrag(link).url
        if link[-1] == '/':
            link = link[:-1]
        url = urlparse(link)

        if not self.check_domain(url):
            # print('FILTERED -- not in specified domain:', url)
            return
        if self.contains_ignored_extension(url):
            # print('FILTERED -- not accepted filetype:', url)
            return
        if self.is_stacked(link):
            # print('FILTERED -- page already stacked:', url)
            return
        if self.is_visited(link):
            # print('FILTERED -- page already visited:', url)
            return

        # if adresa obsahuje clanek, na zacatek fronty
        if url.path.find('/c1-') > -1:
            self.stacked.appendleft(link)
        else:
            self.stacked.append(link)

    def check_domain(self, url):
        if self.domain is None:
            return True
        if url.netloc[-len(self.domain):] == self.domain:
            return True
        return False

    def contains_ignored_extension(self, url):
        for ext in self.ignored_extensions:
            if url.path[-len(ext):] == ext:
                return True
        return False

    def is_visited(self, link):
        return link in self.visited

    def is_stacked(self, link):
        return link in self.stacked

    def get_link(self):
        link = self.stacked.popleft()
        self.visited.append(link)
        return link

    def empty(self):
        return len(self.stacked) == 0
