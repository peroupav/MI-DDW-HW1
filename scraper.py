import requests
from bs4 import BeautifulSoup
from urllib.parse import urlparse, urlunparse, urljoin
from memory import Memory
import time


def scrap_article(soup, outfile):
    h1 = soup.find('h1').text.strip()
    author = soup.find('div', class_ = 'article-info').find('p',class_='author').text.strip()
    time = soup.find('div', class_='article-info').find('p', class_='time').text.strip()
    headlines_html = soup.find('div', class_='headlines')

    outfile.writelines('<h2>{}</h2>'.format(h1))
    outfile.writelines(author)
    outfile.writelines('<br/>')
    outfile.writelines(time)
    outfile.writelines(str(headlines_html))

def crawler(seed, domain, outfile):
    mem = Memory(seed, domain)


    while not mem.empty():
        time.sleep(1)
        link = mem.get_link()
        try:
            print('CRAWLED', link)

            # omezeni dle robots.txt
            if urlparse(link).path.find('/c1-63342510') > -1:
                continue

            source = requests.get(link).text
            soup = BeautifulSoup(source, "html5lib")
            ass = soup.findAll('a', href=True)

            # pokud je prohledana adresa obsahujici clanek, stranka na scrapne
            if urlparse(link).path.find('/c1-') > -1:
                scrap_article(soup, outfile)

            for a in ass:
                href = a['href'].lower()

                href_parsed = urlparse(href)
                if href_parsed.netloc == '':
                    href = urljoin(base=link, url=href)

                mem.add_link(href)

        except Exception as e:
            print(e)
    return


outfile = open('out.html', mode='w', encoding='UTF-8')
outfile.writelines('<!DOCTYPE html><html><head><meta charset="UTF-8"><title>Crawler</title></head><body>')
try:
    crawler('http://domaci.ihned.cz/', 'ihned.cz', outfile)
except KeyboardInterrupt as e:
    outfile.writelines('</body></html>')
    outfile.close()
